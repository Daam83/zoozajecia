package pl.codementors.zoo;

import com.sun.org.apache.bcel.internal.generic.NEW;
import pl.codementors.zoo.animals.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Application main class.
 *
 * @author psysiu
 */
public class ZooMain {

    /**
     * Application starting method.
     *
     * @param args Application starting params.
     */
    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<>();

        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();

                switch (type) {
                    case "Bird": {
                        animals.add(new Bird(name, color));
                        break;
                    }
                    case "Lizard": {
                        animals.add(new Lizard(name, color));
                        break;
                    }
                    case "Mammal": {
                        animals.add(new Mammal(name, color));
                        break;
                    }
                    case "Tiger": {
                        animals.add(new Tiger(name, color));
                        break;

                    }
                    case "Mouse": {
                        animals.add(new Mouse(name, color));
                        break;

                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        // sortuje po nazwie. według komparatora z metody Animal

        Collections.sort(animals);

        print(animals);

        System.out.println("--------");
        // sortuje po komparatorze własnym zdefiniowanym w klasie AnimalComparator.
        Collections.sort(animals, new AnimalComparator()); //
        print(animals);
        System.out.println("~~~~~~~~");
        Set<Animal> animalsSet = new TreeSet<>();
        animalsSet.addAll(animals);
        print(animalsSet);
        System.out.println(animalsSet.size());
        System.out.println("~~~~~~~~");
        Set<Animal> animalsSet1 = new TreeSet<>(new AnimalComparator());
        animalsSet1.addAll(animals);
        print(animalsSet1);
        Set<Mammal>
                mammalSet = new HashSet<>();
        List<Bird>
                birdList = new ArrayList<>();
        Collection<Lizard> lizardCollection =
                new TreeSet<>();
        for (Animal animal : animals
                ) {
            if (animal instanceof Mammal) {
                mammalSet.add((Mammal) animal);
            }
            if (animal instanceof Bird) {
                birdList.add((Bird) animal);
            }
            if (animal instanceof Lizard) {
                lizardCollection.add((Lizard) animal);
            }


        }

        System.out.println("------------");
        System.out.println("Ssaki");
        printCollection(mammalSet);
        System.out.println("Ptaki");
        printCollection(birdList);
        System.out.println("Gady");
        printCollection(lizardCollection);

        Map<String,
                Animal>
                ownersAnimals = new HashMap<>();
        Map<String,
                Animal>
                ownersAnimals1 = new TreeMap<>();
        Map<String,
                Animal>
                ownersAnimals2 = new TreeMap<>(new StringComparator());
        Map<String,
                Animal>
                ownersAnimals3 = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                s.compareTo(t1);
                return t1.compareTo(s);
            }
        });

        Map<String, List<
                Animal>>
                ownersAnimals4 = new TreeMap<>();
        Map<Owner, List<
                Animal>>
                ownersAnimals5 = new HashMap<>();

        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals1.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String ownerAnimal = scanner.next();
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();
                if (ownersAnimals4.containsKey(ownerAnimal) != true) {

                    List<Animal> animalList = new ArrayList<>();

                    switch (type) {
                        case "Bird": {
                            Bird animal = new Bird(name, color);
                            animalList.add(animal);
                            ownersAnimals4.put(ownerAnimal, animalList);
                            break;
                        }
                        case "Lizard": {
                            Lizard animal = new Lizard(name, color);
                            animalList.add(animal);
                            ownersAnimals4.put(ownerAnimal, animalList);
                            break;
                        }
                        case "Mammal": {
                            Mammal animal = new Mammal(name, color);
                            animalList.add(animal);
                            ownersAnimals4.put(ownerAnimal, animalList);
                            break;
                        }
                        case "Tiger": {
                            Tiger animal = new Tiger(name, color);
                            animalList.add(animal);
                            ownersAnimals4.put(ownerAnimal, animalList);
                            break;
                        }
                        case "Mouse": {
                            Mouse animal = new Mouse(name, color);
                            animalList.add(animal);
                            ownersAnimals4.put(ownerAnimal, animalList);
                            break;

                        }
                    }


                } else {

                    switch (type) {
                        case "Bird": {

                            Bird animal = new Bird(name, color);
                            ownersAnimals4.get(ownerAnimal).add(animal);

                            break;
                        }
                        case "Lizard": {
                            Lizard animal = new Lizard(name, color);
                            ownersAnimals4.get(ownerAnimal).add(animal);
                            break;
                        }
                        case "Mammal": {
                            Mammal animal = new Mammal(name, color);
                            ownersAnimals4.get(ownerAnimal).add(animal);
                            break;
                        }
                        case "Tiger": {
                            Tiger animal = new Tiger(name, color);
                            ownersAnimals4.get(ownerAnimal).add(animal);
                            break;
                        }
                        case "Mouse": {
                            Mouse animal = new Mouse(name, color);
                            ownersAnimals4.get(ownerAnimal).add(animal);
                            break;

                        }
                    }
                }

                switch (type) {
                    case "Bird": {
                        ownersAnimals.put(ownerAnimal, (new Bird(name, color)));


                        break;
                    }
                    case "Lizard": {
                        ownersAnimals.put(ownerAnimal, (new Lizard(name, color)));
                        break;
                    }
                    case "Mammal": {
                        ownersAnimals.put(ownerAnimal, (new Mammal(name, color)));
                        break;
                    }
                    case "Tiger": {
                        ownersAnimals.put(ownerAnimal, (new Tiger(name, color)));
                        break;
                    }
                    case "Mouse": {
                        ownersAnimals.put(ownerAnimal, (new Mouse(name, color)));
                        break;

                    }
                }
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        ownersAnimals1.putAll(ownersAnimals);

        System.out.println("---------");

        printByOwners(ownersAnimals);

        System.out.println("-----------");

        printByOwners(ownersAnimals1);

        System.out.println("-----------");

        ownersAnimals2.putAll(ownersAnimals);

        printByOwners(ownersAnimals2);

        System.out.println("-----------");

        ownersAnimals3.putAll(ownersAnimals);

        printByOwners(ownersAnimals3);

        System.out.println("-----------");

        printByOwnersMaps(ownersAnimals4);

        System.out.println("-----------");


        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals1.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String ownerAnimal = scanner.next();
                Owner owner = new Owner();
                owner.setName(ownerAnimal);
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();
                if (ownersAnimals5.containsKey(owner) != true) {

                    List<Animal> animalList = new ArrayList<>();
                    ownersAnimals5.put(owner, animalList);
                }
                    switch (type) {
                        case "Bird": {

                            Bird animal = new Bird(name, color);
                            ownersAnimals5.get(owner).add(animal);

                            break;
                        }
                        case "Lizard": {
                            Lizard animal = new Lizard(name, color);
                            ownersAnimals5.get(owner).add(animal);
                            break;
                        }
                        case "Mammal": {
                            Mammal animal = new Mammal(name, color);
                            ownersAnimals5.get(owner).add(animal);
                            break;
                        }
                        case "Tiger": {
                            Tiger animal = new Tiger(name, color);
                            ownersAnimals5.get(owner).add(animal);
                            break;
                        }
                        case "Mouse": {
                            Mouse animal = new Mouse(name, color);
                            ownersAnimals5.get(owner).add(animal);
                            break;

                        }
                    }
                }



        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        printByOwnersMapsOwnerClass(ownersAnimals5);
        System.out.println("ownerAnimal5");

        System.out.println("---------------");
        Map<Owner, List<
                Animal>>
                ownersAnimals6 = new HashMap<>();

        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals2.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String ownerAnimal = scanner.next();
                String ownerType = scanner.next();

//                Owner.Type.valueOf(ownerType); // zwraca etykiete z enuma
                Owner owner = new Owner();

                owner.setName(ownerAnimal);

                owner.setType(Owner.Type.valueOf(ownerType));

                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();
                if (ownersAnimals6.containsKey(owner) != true) {

                    List<Animal> animalList = new ArrayList<>();
                    ownersAnimals6.put(owner, animalList);

                }

                    switch (type) {
                        case "Bird": {

                            Bird animal = new Bird(name, color);
                            ownersAnimals6.get(owner).add(animal);

                            break;
                        }
                        case "Lizard": {
                            Lizard animal = new Lizard(name, color);
                            ownersAnimals6.get(owner).add(animal);
                            break;
                        }
                        case "Mammal": {
                            Mammal animal = new Mammal(name, color);
                            ownersAnimals6.get(owner).add(animal);
                            break;
                        }
                        case "Tiger": {
                            Tiger animal = new Tiger(name, color);
                            ownersAnimals6.get(owner).add(animal);
                            break;
                        }
                        case "Mouse": {
                            Mouse animal = new Mouse(name, color);
                            ownersAnimals6.get(owner).add(animal);
                            break;

                        }

                    }

            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        printByOwnersMapsOwnerClass(ownersAnimals6);
        System.out.println("ownerAnimals6");
        System.out.println("---------------");

    }
    public static void print(Collection<Animal> animals){
        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }

    public static void printCollection(Collection<? extends Animal> animals) {
        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }

    public static void printByOwners(Map<String, ? extends Animal> animals) {

        for (String owner : animals.keySet()) {
            System.out.println(owner);
            System.out.println(animals.get(owner));
        }
    }

    public static void printByOwnersMaps(Map<String, List<Animal>> animals) {

        for (String animal : animals.keySet()) {
            System.out.println(animal);
            print(animals.get(animal));


        }
    }

    public static void printByOwnersMapsOwnerClass(Map<Owner, List<Animal>> animals) {

        for (Owner animal : animals.keySet()) {
            System.out.print(animal.getName());
            System.out.print(" ");
            if (animal.getType() != null) {
                System.out.print(animal.getType());
            }
            System.out.println(" ");
            print(animals.get(animal));


        }
    }

}
