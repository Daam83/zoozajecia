package pl.codementors.zoo.animals;

/**
 * Just a bird with feathers.
 *
 * @author psysiu
 */
public class Bird extends Animal {

    /**
     * Color of the bird feathers.
     */
    private String featherColor;

    public Bird(String name, String featherColor) {
        super(name);
        this.featherColor = featherColor;
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }

    @Override
    public String toString() {
        return "Bird name = "+super.toString()+" featherColor = "+featherColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bird)) return false;
        Bird bird = (Bird) o;
        if (getName() != getName()) return false;
        return getName().equals(bird.getName()) && getFeatherColor().equals(bird.getFeatherColor());

    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getFeatherColor().hashCode();
    }

    @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Bird) {
                return featherColor.compareTo(((Bird) o).featherColor);


            }
        }
        return ret;
    }
}
