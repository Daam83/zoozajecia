package pl.codementors.zoo.animals;

import lombok.*;


/**
 * Created by student on 15.06.17.
 */
@EqualsAndHashCode
public class Owner {
    private
    @Getter
    @Setter
    String name;
    @Setter
    @Getter
    Type type;

    public enum Type {BREEDER, COLLECTOR}

    ;


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Owner owner = (Owner) o;
//
//        return name != null ? name.equals(owner.name) : owner.name == null;
//    }
//
//    @Override
//    public int hashCode() {
//        return name != null ? name.hashCode() : 0;
//    }
}
