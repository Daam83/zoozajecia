package pl.codementors.zoo.animals;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * Just an animal.
 *
 * @author psysiu
 */
public class Animal implements Comparable<Animal> {

    /**
     * Animal name.
     */
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {

        return name;
    }


    @Override
    public int compareTo(Animal animal) {
        int ret = name.compareTo(animal.getName());
        if (ret == 0) {
            return getClass().getCanonicalName().compareTo(animal.getClass().getCanonicalName());
        }
        return ret; // bierze nazwę i porównuje je z kolejną nazwą kolejnego obiektu

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Animal)) return false;
        Animal animal = (Animal) o;
        if (getName() != animal.getName()) return false;
        return getName().equals(animal.getName());
    }

    //    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Mage)) return false;
//
//        Mage mage = (Mage) o;
//
//        if (getPower() != mage.getPower()) return false;
//        return getName().equals(mage.getName());
//    }

    @Override
    public int hashCode() {
        int result = getClass().getCanonicalName().hashCode(); // nie robić hashCode z getClass() bo hash jest tworzony z referencji. za każdym uruchomieniem będzi inny.
        result = 31 * result + getClass().getName().hashCode();

        return result;
        // OR

//        @Override
//        public int hashCode() {
//
//           return = 31 * name.hashCode();
//
//        }
    }

}
