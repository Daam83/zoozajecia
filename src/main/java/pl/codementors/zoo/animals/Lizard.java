package pl.codementors.zoo.animals;

/**
 * Lizard with some scales.
 *
 * @author psysiu
 */
public class Lizard extends Animal {

    /**
     * Color of the animal scales.
     */
    private String scalesColor;

    public Lizard(String name, String scalesColor) {
        super(name);
        this.scalesColor = scalesColor;
    }

    public String getScalesColor() {
        return scalesColor;
    }

    public void setScalesColor(String scalesColor) {
        this.scalesColor = scalesColor;
    }

    @Override
    public String toString() {
        return "Lizard name = "+super.toString()+" scalesColor = "+ scalesColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lizard)) return false;
        Lizard lizard = (Lizard) o;
        if (getName() != getName()) return false;
        return getName().equals(lizard.getName()) && getScalesColor().equals(lizard.getScalesColor());

    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getScalesColor().hashCode();
    }

    @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Lizard) {
                return scalesColor.compareTo(((Lizard) o).scalesColor);

            }
        }
        return ret;
    }
}
