package pl.codementors.zoo.animals;

/**
 * Just a mammal with some fur.
 *
 * @author psysiu
 */
public class Mammal extends Animal {

    /**
     * Color of the animal fur.
     */
    private String furColor;

    public Mammal(String name, String furColor) {
        super(name);
        this.furColor = furColor;
    }

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    @Override
    public String toString() {
        return "Mammal name = "+super.toString()+" furColor = "+ furColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mammal)) return false;
        Mammal mammal = (Mammal) o;
        if (getName() != getName()) return false;
        return getName().equals(mammal.getName()) && getFurColor().equals(mammal.getFurColor());

    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getFurColor().hashCode();
    }

    @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Mammal) {
                return furColor.compareTo(((Mammal) o).furColor);

            }
        }
        return ret;
    }
}
