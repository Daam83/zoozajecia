package pl.codementors.zoo.animals;

/**
 * Created by student on 13.06.17.
 */
public class Tiger extends Mammal{
    public Tiger(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {
        return "Tiger name = "+getName()+" furColor = "+ getFurColor();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tiger)) return false;
        Tiger tiger = (Tiger) o;
        if (getName() != getName()) return false;
        return getName().equals(tiger.getName()) && getFurColor().equals(tiger.getFurColor());

    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getFurColor().hashCode();
    }

//    @Override
//    public int compareTo(Animal o) {
//        int ret = super.compareTo(o);
//        if(ret ==0){
//            if (o instanceof Mammal){
//                return
//            }
//        }
//
//        return super.compareTo(o);
//    }
}
