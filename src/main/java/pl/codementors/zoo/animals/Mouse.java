package pl.codementors.zoo.animals;

/**
 * Created by student on 13.06.17.
 */
public class Mouse extends Mammal{
    public Mouse(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {

        return "Mouse [name = "+getName()+ " furColor = "+getFurColor()+"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mouse)) return false;
        Mouse mouse = (Mouse) o;
        if (getName() != getName()) return false;
        return getName().equals(mouse.getName()) && getFurColor().equals(mouse.getFurColor());

    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getFurColor().hashCode();
    }
}
