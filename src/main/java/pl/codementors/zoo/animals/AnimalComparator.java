package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by student on 13.06.17.
 */
public class AnimalComparator implements Comparator<Animal> {


    @Override
    public int compare(Animal a1, Animal a2) { // porównuje dwa obiekty

        if (a1.getClass().getSimpleName().compareTo(a2.getClass().getSimpleName()) == 0) {// Może się zdażyć, że typy
            // są takie same dlatego bedzie zwracał 0
            // dlatego trzeba porównać po imieniu.
            return a1.getName().compareTo(a2.getName()); //  porównuje nazwy jako String
        } else {
            return a1.getClass().getSimpleName().compareTo(a2.getClass().getSimpleName());
        }


    }
}
